#!/usr/bin/perl

use strict;
use warnings;
use Data::Validate::IP 'is_ipv4';
use Getopt::Long;
use LWP::UserAgent;
use Net::Netmask;

my ($country, $file, $filter, $filter_out, $max_connection, $speed, $url);

GetOptions(
    'country=s'       => \$country,
    'file=s'          => \$file,
    'filter=s'        => \$filter,
    'filter-out=s'    => \$filter_out,
    'maxconnection=s' => \$max_connection,
    'speed=s'         => \$speed,
    'url=s'           => \$url,
);

unless (defined $url || defined $file) {
    usage('Option -url or -file need to be specified');
}

if (defined $file && defined $url) {
    usage('Option -file and -url can\'t be specified at the same time');
}

my $list;

if (defined $url) {
    $list = get_list_http($url);
}

if (defined $file) {
    $list = do { local $/ = undef; open my $fh, '<', $file; <$fh> };
}

unless (defined $max_connection || defined $speed) {
    usage('Option -speed or -maxconnection need to be specified');
}

if (defined $filter) {
    $filter = qr/$filter/;
}

if (defined $filter_out) {
    $filter_out = qr/$filter_out/;
}

open my $fh, '>', 'throttle.conf' or die "cannot open > throttle.conf $!";;
print $fh "<IfModule mod_bw.c>\n\tBandwidthModule On\n\tForceBandWidthModule On\n";
my $text = '';

for my $line (split "\n", $list) {
    # Skip comments
    next if (substr($line, 0, 1) eq '#');

    if (defined $country) {
        my $country_of_line = get_country($line);
        next if ($country_of_line ne $country);
    }

    next if ((defined $filter && $line !~ /$filter/) || (defined $filter_out && $line =~ /$filter_out/));

    my ($start, $end) = get_start_ip_to_end_ip($line);
    next unless (is_ipv4($start) && is_ipv4($end));

    my @blocks = new2 Net::Netmask (join ' - ', ($start, $end));
    next unless defined $blocks[0];
    if (defined $speed) {
        for my $block (@blocks) {
            $text .= "\tBandwidth " . $block->base() . '/' . $block->bits() . " $speed" . "\n";
        }
    }
    if (defined $max_connection) {
        for my $block (@blocks) {
            $text .= "\tMaxConnection " . $block->base() . '/' . $block->bits() . " $max_connection" . "\n";
        }
    }
}

print $fh $text;
print $fh "\tMinBandwidth all -1\n</IfModule>";
close $fh;

sub get_country {
    my $raw_line = shift;
    my $country = (split /\|/, $raw_line)[2];

    return $country;
}

sub get_list_http {
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    $ua->env_proxy;
    my $response = $ua->get($url);

    unless ($response->is_success) {
        die 'Could not download list:' . $response->status_line;
    }

    return $response->decoded_content;
}

sub get_start_ip_to_end_ip {
    my $raw_line = shift;
    my ($start, $end) = (split /\|/, $raw_line)[0 .. 1];

    return ($start, $end)
}

sub usage {
    my $message = shift;
    $message .= "\n"
        if (defined $message && $message !~ /\n$/);
    my $command = $0;
    $command =~ s/^.*\///;
    print(
        $message . "usage:  $command -url URL -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]\n"
                 . "        $command -file FILE -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]\n");
    die ("\n");
}

=head1 DESCRIPTION

This script takes as argument ranges of IP addresses and outputs a configuration
file suitable for Apache's L<mod_bw|https://svn.apache.org/repos/asf/httpd/sandbox/mod_bw/mod_bw.txt>

=head1 USAGE

    perl -url URL -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]
    perl -file FILE -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]

=head3 Input

Ranges of IP addresses in this format:

    start|end|country|class|name

For example:

    192.168.1.0|192.168.1.255|EN|LAN|Wifi LAN

The new line character is C<\n>.

=head3 Output

The output will look like the following configuration:

    <IfModule mod_bw.c>
    	BandwidthModule On
    	ForceBandWidthModule On
    	MaxConnection 6.0.0.0/16 500
    	MinBandwidth all -1
    </IfModule>


=head3 Parameters

=over

=item C<country>

This parameter filters a country in the list of IP ranges.

=item C<filter>

This parameter filters the list of IP ranges according to a regex.

=item C<filter-out>

This parameter filters out the list of IP ranges according to a regex.

=item C<maxconnection>

This parameter specifies how many connections from a given IP range is
authorized.

=item C<speed>

This parameter specifies the speed (in bytes/s) of each client in the IP ranges.

=item C<url>

This parameter specifies the url to download the list of IP ranges.

=item C<file>

This parameter specifies the fie which contains the list of IP ranges.

=back

=cut
