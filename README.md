# DESCRIPTION

This script takes as argument ranges of IP addresses and output a configuration
file suitable for Apache's [mod\_bw](https://svn.apache.org/repos/asf/httpd/sandbox/mod_bw/mod_bw.txt)

# USAGE

    perl -url URL -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]
    perl -file FILE -speed SPEED maxconnection MAXCONNECTION [-filter REGEX] [-filter-out REGEX] [-country COUNTRY]

### Input

Ranges of IP addresses in this format:

    start|end|country|class|name

For example:

    192.168.1.0|192.168.1.255|EN|LAN|Wifi LAN

The new line character is `\n`.

### Output

The output will look like the following configuration:

    <IfModule mod_bw.c>
        BandwidthModule On
        ForceBandWidthModule On
        MaxConnection 6.0.0.0/16 500
        MinBandwidth all -1
    </IfModule>

### Parameters

- `country`

    This parameter filters a country in the list of IP ranges.

- `filter`

    This parameter filters the list of IP ranges according to a regex.

- `filter-out`

    This parameter filters out the list of IP ranges according to a regex.

- `maxconnection`

    This parameter specifies how many connections from a given IP range is
    authorized.

- `speed`

    This parameter specifies the speed (in bytes/s) of each client in the IP ranges.

- `url`

    This parameter specifies the url to download the list of IP ranges.

- `file`

    This parameter specifies the fie which contains the list of IP ranges.
